import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import api from './services/api';

import './App.css';
import Nav from './components/Nav';
import TarefaList from './components/TarefaList';
import TarefaDetail from './components/TarefaDetail';
import TarefaEdit from './components/TarefaEdit';

function App() {

  return (
    <div id="App">
      <Nav />      
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/tarefa/:id" exact component={TarefaDetail} />     
          <Route path="/tarefa/edit/:id" exact component={TarefaEdit} />
        </Switch>
      </Router>
    </div>
  );
}

const Home = () => {
  const [tarefas, setTarefas] = useState([]);
  
  useEffect(() => {
    async function loadTarefas() {
      const response = await api.get('/tarefas');

      setTarefas(response.data); 
    }

    loadTarefas();
  }, []);

  return (
    <div className="container mt-4">
      <div className="d-flex justify-content-center">
        <div className="col-md-11">           
    
          {tarefas.map(tarefa => (
              <TarefaList key={tarefa.id} tarefa={tarefa} />
          ))}

        </div>
      </div>
    </div> 
  )  
};

export default App;
