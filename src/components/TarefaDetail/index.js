import React, { useState, useEffect } from 'react';
import  { Link } from 'react-router-dom'
import api from '../../services/api';
import Moment from 'moment';

import './style.css';

function TarefaDetail({ match }) {
  const [tarefa, setTarefa] = useState({}); 
   
  useEffect(() => {
    async function getTarefa() {
      const response = await api.get(`/tarefa/${match.params.id}`);

      setTarefa(response.data); 
    }

    getTarefa();
  }, []);

  async function deleteTarefa(id) {    
    const confirm = window.confirm('Deseja excluir esta tarefa?');

    if(confirm) {
      const response = await api.delete(`/tarefa/${id}`);
      alert(response.data);  
      
      window.location = '/';
    }    
  }

  return (      
    <div className="container">     
      <ul>           
        <li>
        <Link to='/'>
          <button className="btn btn-secondary">
            Voltar
          </button>
        </Link>  
        </li>
        <li><h1>Título: {tarefa.titulo}</h1></li>
        <li><b>Descrição:</b> {tarefa.descricao}</li>
        <li><b>Responsável:</b> {tarefa.responsavel}</li>
        <li><b>Status:</b> {tarefa.status}</li>
        <li><b>Data de entrega:</b> {Moment(tarefa.data_entrega).format('DD/MM/Y')}</li>
        <li>                             
          <Link to={`/tarefa/edit/${tarefa.id}`}>
            <button className="btn btn-primary">
              Editar
            </button>
          </Link>
          <button className="btn btn-danger" onClick={deleteTarefa.bind(this, tarefa.id)}>Deletar</button>                               
        </li>        
      </ul>    
    </div>                
  );
}

export default TarefaDetail;