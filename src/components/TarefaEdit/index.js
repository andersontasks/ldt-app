import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import api from '../../services/api';

import './style.css';

function TarefaEdit({ match }) {
    const [titulo, setTitulo] = useState('');
    const [descricao, setDescricao] = useState('');
    const [responsavel, setResponsavel] = useState('');
    const [data_entrega, setDataEntrega] = useState('');
    const [status, setStatus] = useState('');
       
    useEffect(() => {
      async function getTarefa() {
        await api.get(`/tarefa/${match.params.id}`).then(response => {
            setTitulo(response.data.titulo);
            setDescricao(response.data.descricao);
            setResponsavel(response.data.responsavel);
            setDataEntrega(response.data.data_entrega);
            setStatus(response.data.status);
        },
        (err) => {
            console.log(err);
        });        
      }
  
      getTarefa();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();      
        
        const tarefa = {
            titulo: titulo,
            descricao: descricao,
            responsavel: responsavel,
            data_entrega: data_entrega,
            status: status,
        };

        await api.put(`/tarefa/${match.params.id}`, {tarefa}).then(result => {
            alert(result.data)
        },
        (err) => {
            alert(err);
        });
    }

    return (
        <div className="container">    
            <Link to='/'>
              <button className="btn btn-secondary mb-3">
                Voltar
              </button>
            </Link>      

            <h1 className="mb-3">Editar tarefa</h1>

            <form onSubmit={handleSubmit}>                
                <div className="form-group">
                    <label htmlFor="titulo">Título</label>
                    <input 
                    type="text" 
                    name="titulo" 
                    id="titulo" 
                    className="form-control" 
                    required
                    value={titulo} 
                    onChange={e => setTitulo(e.target.value)} 
                    />
                </div>

                <div className="form-group">
                <label htmlFor="descricao">Descrição</label>
                    <textarea 
                    name="descricao" 
                    className="form-control" 
                    id="descricao" 
                    rows="3" 
                    required
                    value={descricao}
                    onChange={e => setDescricao(e.target.value)} 
                    ></textarea>                    
                </div>

                <div className="form-group">
                <label htmlFor="responsavel">Responsável</label>
                    <input 
                    type="text" 
                    name="responsavel" 
                    className="form-control" 
                    id="responsavel" 
                    required
                    value={responsavel} 
                    onChange={e => setResponsavel(e.target.value)} 
                    />
                </div>

                <div className="form-group">
                <label htmlFor="data_entrega">Data de entrega</label>
                    <input 
                    type="date" 
                    name="data_entrega" 
                    className="form-control" 
                    id="data_entrega" 
                    required
                    value={data_entrega} 
                    onChange={e => setDataEntrega(e.target.value)} 
                    />
                </div>

                <div className="form-group">
                    <label htmlFor="status">Status</label>
                    <select 
                    name="status" 
                    className="form-control" 
                    id="status" 
                    required
                    value={status} 
                    onChange={e => setStatus(e.target.value)} 
                    >
                        <option value='Concluído'>Concluído</option>
                        <option value='Pendente'>Pendente</option>
                    </select>                    
                </div>

                <button type="submit" className="btn btn-success">Salvar</button>
            </form>
        </div>
    );
}

export default TarefaEdit;

