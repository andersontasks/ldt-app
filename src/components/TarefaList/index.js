import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

function TarefaList({ tarefa }) {
    return (
        <Link to={`tarefa/${tarefa.id}`} title="Visualizar">
            <div className="card">
                <div className="card-header">{tarefa.titulo}</div>
                <div className="card-body">                
                    <p className="card-subtitle mb-2"><b>Status:</b> {tarefa.status}</p>
                    <p className="card-text">{tarefa.descricao}</p>                           
                </div>               
            </div>
        </Link>
    );
}

export default TarefaList;